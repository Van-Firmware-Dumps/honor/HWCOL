precision mediump float;


uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

attribute vec3 position;
//attribute vec3 color;
attribute vec3 normal;
attribute vec2 uv;

attribute vec4 skinIndex;
attribute vec4 skinWeight;


varying vec2 vs_out_uv; 
uniform vec4 offsetRepeat;  
 
uniform mat4 boneMatrices[14];  
uniform int boneCount; 
  
void main() {  
    vs_out_uv = uv * offsetRepeat.zw + offsetRepeat.xy;  
  
    mat4 boneMatX = boneMatrices[ int(skinIndex.x) ];  
    mat4 boneMatY = boneMatrices[ int(skinIndex.y) ];  
    mat4 boneMatZ = boneMatrices[ int(skinIndex.z) ];  
    mat4 boneMatW = boneMatrices[ int(skinIndex.w) ]; 
 
    vec4 skinVertex = vec4( position, 1.0 );  
  
    vec4 skinned = vec4( 0.0 );  
    skinned += boneMatX * skinVertex * skinWeight.x;  
    if (boneCount > 1) skinned += boneMatY * skinVertex * skinWeight.y;  
    if (boneCount > 2) skinned += boneMatZ * skinVertex * skinWeight.z;  
    if (boneCount > 3) skinned += boneMatW * skinVertex * skinWeight.w;  
  
    gl_Position = projectionMatrix * modelViewMatrix * skinned;  
}