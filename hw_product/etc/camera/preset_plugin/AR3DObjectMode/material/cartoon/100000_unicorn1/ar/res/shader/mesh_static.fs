precision mediump float;
 
varying vec2 vs_out_uv;  
uniform sampler2D map;  
 
void main() { 
    gl_FragColor = texture2D(map, vs_out_uv);
}
