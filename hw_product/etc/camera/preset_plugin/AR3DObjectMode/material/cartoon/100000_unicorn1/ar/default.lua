app_controller = ae.ARApplicationController:shared_instance()
app_controller:require('./scripts/include.lua')
app = AR:create_application(AppType.Slam, "cat")
app:load_scene_from_json("res/simple_scene.json","demo_scene")
lua_handler = app:get_lua_handler()
scene = app:get_current_scene()
update = 0
touch = 0
show_gesture_guide_move = false
show_gesture_guide_rotate = false
show_gesture_guide_singlemove = false
app.on_loading_finish = function()
  -- 开启离屏引导
  scene:set_show_offscreen_guidance(true)
  scene:set_slam_move_limits(0, 5000)
  click_Go()

  scene:set_offscreen_guidance_target("cat_group")
end


function click_Go()


  scene.cat1before:set_visible(true)


  cat1before = scene.cat1before:pod_anim()
        :repeat_count(-1)

        :on_complete(function ()

        end)
        :start()

end



local show_offscreen_button = false
local has_dismis_guide = false
local has_show_arrow = false
local has_show_slam_guide = false

scene.reset.on_click = function()
    ARLOG('reset on_click')
    app:relocate_current_scene()
    app.slam:slam_reset(0.5,0.5,1000,3)
    reset_slam_with_face_to_camera(100)
end

function reset_slam_with_face_to_camera(delay)
    first_reset = true
    function reset_slam_face_to_camera()
        app.slam:slam_reset(0.5,0.5,1000,3)
    end
    if (first_reset) then
        first_reset = false
        app.slam:slam_reset(0.5,0.5,1000,3)
        AR:perform_after(delay, reset_slam_face_to_camera)
    else
        app.slam:slam_reset(0.5,0.5,1000,3)
    end
end

app.offscreen_button_show = function()
    ARLOG('offscreen_button_show')
    show_offscreen_button = true
    if ((not has_show_arrow) and (not has_show_slam_guide)) then
        has_show_arrow = true
      --  scene.arrow_guide:set_visible(true)
        ae.LuaUtils:call_function_after_delay(3000, "hide_arrow_guide")
    else
        scene.reset:set_visible(true)
        frame_id = scene.reset:framePicture()
            :repeat_count(-1)
            :start()
    end
end

app.offscreen_button_hide = function()
    ARLOG('offscreen_button_hide') 
    show_offscreen_button = false
    scene.reset:set_visible(false)
   -- scene.arrow_guide:set_visible(false)
    if (not has_dismis_guide) then
        has_dismis_guide = true
    end
end



function hide_arrow_guide()
    ARLOG('hide_arrow_guide')
    if (not has_dismis_guide) then 
       -- scene.arrow_guide:set_visible(false)
        scene.reset:set_visible(true)
                frame_id = scene.reset:framePicture()
            :repeat_count(-1)
            :start()
    end
end











