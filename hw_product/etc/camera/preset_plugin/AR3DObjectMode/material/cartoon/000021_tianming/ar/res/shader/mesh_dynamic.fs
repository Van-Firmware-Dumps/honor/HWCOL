precision mediump float;

uniform vec3 diffuse;  
uniform float opacity;  
uniform float global_time;
 
varying vec2 vs_out_uv;  
uniform sampler2D map;  
uniform sampler2D specularMap;
 
void main() { 
    vec2 uv0 = vec2(vs_out_uv.x, 1.0 - vs_out_uv.y);
    
    float uv_scale = 0.4;
    vec2 uv1 = uv0 + global_time * uv_scale;

    vec4 color0 = texture2D( map, uv0 );
    vec4 color1 = texture2D( specularMap, uv1 );

    gl_FragColor.rgb = color0.rgb * (1.0 - color1.a) + color1.rgb * color1.a;
    gl_FragColor.a = color0.a;
}
